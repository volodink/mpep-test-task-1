.PHONY: all clean live 

all: clean
	java -jar .devcontainer/lib/plantuml.jar src/diagrams/*.puml
	marp --html src/mpep-test-task-1.md -o build/mpep-test-task-1.pdf

live:
	marp --html --server ./src/

clean:
	rm -rf build
	rm -rf builds
